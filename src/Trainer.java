import java.util.ArrayList;
import java.util.Date;

public class Trainer {
    protected String Name;
    private String Email;
    private String Password;
    private enums.gender Gender;
    private Date PlaySince;
    ArrayList<pokedex> PokeList = new ArrayList<pokedex>();

    public void DisplayBio() {
        System.out.println(
                "Name = " + this.Name +
                        "\nEmail = " + this.Email +
                        "\nGender = " + this.Gender +
                        "\nStart Since = " + this.PlaySince +
                        "\nPokemon caught = " + this.PokeList.size() + "\n\n");
    }

    public void DisplayPokedex() {
        String Temp = "Pokedex of " + Name + " ";
        System.out.print(Temp);
        for (int i = 0; i < 50 - Temp.length(); i++) {
            System.out.print("=");
        }
        System.out.print("\n");
        PokeList.forEach((n) -> n.Display());
        System.out.println("\n==================================================");

    }

    public String getName() {
        return this.Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getEmail() {
        return this.Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public enums.gender getGender() {
        return this.Gender;
    }

    public void setGender(enums.gender Gender) {
        this.Gender = Gender;
    }

    public Date getPlaySince() {
        return this.PlaySince;
    }

    public void setPlaySince(Date PlaySince) {
        this.PlaySince = PlaySince;
    }

    public void addPokemon(pokedex NewPokemon) {
        this.PokeList.add(NewPokemon);
    }
}

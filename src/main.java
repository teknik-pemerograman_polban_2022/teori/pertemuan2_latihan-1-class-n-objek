import java.util.Date;

public class main {
    public static void main(String[] args) {
        pokedex Bulbasaur = new pokedex();
        Bulbasaur.Name = "Bulbasaur";
        Bulbasaur.Height = 2.04;
        Bulbasaur.Weight = 15.2;
        Bulbasaur.setCategory("Seed");
        Bulbasaur.setAbilities("Overgrow");
        Bulbasaur.setType(enums.element.Grass);
        Bulbasaur.setType(enums.element.Poison);
        Bulbasaur.setWeakness(enums.element.Fire);
        Bulbasaur.setWeakness(enums.element.Psychic);
        Bulbasaur.setWeakness(enums.element.Flying);
        Bulbasaur.setWeakness(enums.element.Ice);
        Bulbasaur.setGender(enums.gender.female);

        pokedex Pikachu = new pokedex();
        Pikachu.Name = "Pikachu";
        Pikachu.Height = 1.04;
        Pikachu.Weight = 13.2;
        Pikachu.setCategory("Mouse");
        Pikachu.setAbilities("Static");
        Pikachu.setType(enums.element.Electric);
        Pikachu.setWeakness(enums.element.Ground);
        Pikachu.setGender(enums.gender.male);

        Trainer Ash = new Trainer();
        Ash.setName("Ash Ketchum");
        Ash.setEmail("ashketchum@gmail.com");
        Ash.setPassword("Lets go Pikachu!");
        Ash.setGender(enums.gender.male);
        Ash.setPlaySince(new Date("1/1/2020 14:15"));

        Ash.addPokemon(Bulbasaur);
        Ash.addPokemon(Pikachu);

        Ash.DisplayBio();
        Ash.DisplayPokedex();

    }
}

import java.util.ArrayList;

public class pokedex {
    String Name;
    double Height;
    double Weight;
    protected String Category;
    protected String Abilities;
    private enums.gender Gender;
    protected ArrayList<enums.element> Type = new ArrayList<enums.element>();
    protected ArrayList<enums.element> Weakness = new ArrayList<enums.element>();

    public void Display() {
        System.out.println(
                "\n////////////////////////////" +
                        "\nPokemon Name      = " + this.Name +
                        "\n------" +
                        "\nHeight            = " + this.Height + " feet" +
                        "\nWeight            = " + this.Weight + " lbs" +
                        "\nGender            = " + this.Gender +
                        "\n---" +
                        "\nCategory          = " + this.Category +
                        "\nAbilities         = " + this.Abilities +
                        "\nType              = " + this.getType() +
                        "\nWeakness          = " + this.getWeakness() +
                        "\n////////////////////////////"

        );
    }

    public ArrayList<enums.element> getType() {
        return this.Type;
    }

    public void setType(enums.element TypeInput) {
        this.Type.add(TypeInput);
    }

    public ArrayList<enums.element> getWeakness() {
        return this.Weakness;
    }

    public void setWeakness(enums.element WeaknessInput) {
        this.Weakness.add(WeaknessInput);
    }

    public String getCategory() {
        return this.Category;
    }

    public void setCategory(String Category) {
        this.Category = Category;
    }

    public String getAbilities() {
        return this.Abilities;
    }

    public void setAbilities(String Abilities) {
        this.Abilities = Abilities;
    }

    public enums.gender getGender() {
        return this.Gender;
    }

    public void setGender(enums.gender Gender) {
        this.Gender = Gender;
    }

}

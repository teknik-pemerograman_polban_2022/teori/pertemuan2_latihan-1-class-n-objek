public class enums {
    public enum element {
        Normal, Fire, Water, Grass, Flying, Fighting, Poison, Electric, Ground, Rock, Psychic, Ice, Bug, Ghost, Steel,
        Dragon, Dark, Fairy
    }

    public enum gender {
        male, female
    }
}
